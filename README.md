#Peticlette

Comme tout les fichiers sont compilés dans ce projet, les sources sont sur ce [repo](https://bitbucket.org/colinpeyrat/ddi_peticlette_app) et les sources de l'API [ici](https://bitbucket.org/colinpeyrat/ddi_peticlette_server) 

##Installation
Ce projet corodva utilise [cordova-icon](https://github.com/AlexDisler/cordova-icon) pour générer automatiquement les icônes. Il se lance à chaque `cordova build` ou `cordova run <platform>`

Pour l'installer : 

```
$ sudo apt-get install imagemagick
$ # on Mac: brew install imagemagick
$ # on Windows: http://www.imagemagick.org/script/binary-releases.php#windows

$ sudo npm install cordova-icon -g
```

Bien veiller à ce qu'il ai les droits :

```
chmod +x hooks/after_prepare/cordova-icon.sh
```

ensuite,

```
cordova platform add android
```

et enfin : 

```
cordova run android
```

## Utilisation

Comme l'API est hébergée sur un serveur Heroku, et que le serveur Node n'a pas de persistance de données. J'ai créé un compte de test :
- login: `Peticlette`
- password: `peticlette`
